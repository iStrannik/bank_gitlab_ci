const yaml = require('js-yaml');

import { Payment } from './payment';
import { Transation } from './transation';

import { sql_client } from './sql_cli'
import { s3_client } from './s3_cli';
import { proxy_client } from './proxy_cli';

var TranssactionYamlType = new yaml.Type('!TranssactionYamlType', {
    kind: 'sequence',

    resolve: function (data: null) {
        return data !== null;
    },

    // If a node is resolved, use it to create a Point instance.
    construct: function (data: any[]) {
        let payments : Payment[] = []
        data.forEach((element: {
            destination: string;
            amount: any; source: string; 
}) => {
            payments.push(new Payment(element.source, element.destination, element.amount))
        });
        return new Transation(payments);
    },

    instanceOf: Transation,
});

let TRANSACTION_SCHEMA = yaml.Schema.create([TranssactionYamlType]);

export function get_payments_from_file(user_id: string, file_content: any) {
    try {
        var transaction : Transation = yaml.safeLoad(file_content, { schema: TRANSACTION_SCHEMA });
    } catch (error) {
        return null
    }
    return sql_client.process_transaction(user_id, transaction);
};

export function get_payments_from_s3(user_id: string, file_id: string) {
    return get_payments_from_file(user_id, s3_client.get_file(user_id, file_id));
};

export function get_payments_from_url(user_id: string, url: string) {
    return get_payments_from_file(user_id, proxy_client.dowload_file(url));
};