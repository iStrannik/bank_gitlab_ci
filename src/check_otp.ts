import { sql_client } from "./sql_cli";

export function check_otp(user_id, transaction_id, code) {
    var res = sql_client.get_otp(user_id, transaction_id);
    var currentdate = new Date();
    if (currentdate.getTime() - res.date >= 60 || res.otp != code) {
        return false;
    }
    return true;
}