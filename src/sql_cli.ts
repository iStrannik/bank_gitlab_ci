// basctract sql cli
const {
    Client
} = require('pg')

import {
    v4 as uuidv4
} from 'uuid';
import {
    Transation
} from './transation';

import {
    Payment
} from './payment';

class SQLClient {
    client: typeof Client
    construct() {
        const client = new Client()
        client.connect()
    }

    process_transaction(user_id: string, transaction: Transation) {
        let transaction_id = this.insert_transaction(user_id);
        transaction.payments.forEach((payment) => {
            this.insert_payment(transaction_id, payment)
        })
        return transaction_id
    }

    insert_payment(transaction_id: string, payment: Payment) {

        this.client.query('INSERT INTO PAYMENTS VALUES(? , ?, ?, ?)', [transaction_id, payment.source, payment.destination, payment.amount], (err, res) => {
            this.client.end()
        })

    }

    get_transaction(user_id: string, transaction_id: string) {
        this.client.query('SELECT * FROM TRANSACTIONS WHERE user_id = ? ans transaction_id = ?', [user_id, transaction_id], (err, res) => {
            if (res.isEmpty()) {
                throw Error('Access denied')
            }
            this.client.end()
        })

        this.client.query('SELECT * FROM PAYMENTS WHERE transaction_id = ?', [transaction_id], (err, res) => {
            this.client.end()
            return res
        })

        return this.client.query('SELECT * FROM PAYMENTS WHERE transaction_id = ?', [transaction_id], (err, res) => {
            this.client.end()
            return res
        })
    }

    insert_transaction(user_id: string) {
        let uuid = uuidv4();
        while (!this.check_key(uuid)) {
            uuid = uuidv4();
        }

        this.client.query('INSERT INTO TRANSACTIONS VALUES(? , ?)', [user_id, uuid], (err, res) => {
            this.client.end()
        })

        return uuid
    }

    check_key(uuid: string) {
        // check key not exist in table
        return true
    }

    get_otp(user_id, transaction_id) {
        return this.client.query('SELECT TIME_ASKED, OTP FROM OTPS WHERE user_id = ? ans transaction_id = ?', [user_id, transaction_id], (err, res) => {
            this.client.query('DELETE FROM OTPS WHERE user_id = ? ans transaction_id = ?', [user_id, transaction_id], (err, res) => { })
            this.client.end()
            return res
        });
    }
}

export let sql_client = new SQLClient();
