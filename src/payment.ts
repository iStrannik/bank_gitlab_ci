const Big = require('big.js');

export class Payment {
    source : Account;
    destination : Account;
    amount : typeof Big;
    constructor (source : string, destination : string, amount : any) {
        this.source      = new Account(source);
        this.destination = new Account(destination);
        this.amount      = new Big(amount).toPrecision(2);
   }
}

class Account {
    account : string
    constructor (account) {
        this.validate(account)
        this.account = account;
    }

    validate(account) {
        const schema = /^\d{20}$/;
        if (!account.match(schema)) {
            throw Error('Bad account')
        }
    }
}