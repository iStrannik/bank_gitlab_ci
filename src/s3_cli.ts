import urlJoin from 'url-join';
var https = require('https');

class S3Client {
    base_url : string
    construct() {
        this.base_url = 'https://s3.kek.ru'
    }

    get_file(user_id: string, file_id: string) {
        this.check_access(user_id, file_id);
        return this.dowload_file(file_id)
    }

    check_access(user_id, file_id) {
        let url = urlJoin(this.base_url, encodeURIComponent(file_id));
        // check 
        return true
    }

    dowload_file(file_id) {
        let url = urlJoin(this.base_url, encodeURIComponent(file_id));
        return https.get(url)
    }
    
}

export let s3_client = new S3Client();
