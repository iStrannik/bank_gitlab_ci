var https = require('https');

class ProxyClient {
    base_url: string
    construct() {
        this.base_url = 'https://proxyapi.kek.ru'
    }

    dowload_file(file_url) {
        let options = {
            url: this.base_url,
            params: {
                'url': file_url
            }
        }
        return https.get(options)
    }
}

export let proxy_client = new ProxyClient();