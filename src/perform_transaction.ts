const { spawn } = require('node:child_process');

import { Payment } from "./payment";
import { sql_client } from "./sql_cli";

export function perform_transaction(user_id, transaction_id) {
    var transactions = sql_client.get_transaction(user_id, transaction_id);
    transactions.array.forEach(element => {
        try {
            new Payment(element.source, element.destination, element.amount);
        } catch(error) {
            throw Error('Bad payment');
        }
        spawn('transfer', [element.source, element.destination, element.amount])
    });
}